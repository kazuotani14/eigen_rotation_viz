#include <iostream>
#include <string>
#include <cmath>

#include <Eigen/Core>
#include <Eigen/Geometry>

#include <vector>
#include <cmath>
#include <boost/tuple/tuple.hpp>
#include "../thirdparty/gnuplot-iostream/gnuplot-iostream.h"

using namespace std;
using namespace Eigen;

/*
    Simple plotting tool for testing Eigen rotations - Matrix3, AngleAxis, Quaternion
    Plots given rotation frame
*/
void plot_rotation(Gnuplot& gp, const string name, const Matrix3d& m, int plot_orientation=1)
{
    std::vector<boost::tuple<double, double, double> > x_axis, y_axis, z_axis; //these are lines
    x_axis.push_back(boost::make_tuple(0,0,0));
    y_axis.push_back(boost::make_tuple(0,0,0));
    z_axis.push_back(boost::make_tuple(0,0,0));

    if(plot_orientation==0)
    {
        // For default gnuplot axes: x right, y backwards (into depth), z up
        x_axis.push_back(boost::make_tuple(m(0,0),m(1,0),m(2,0)));
        y_axis.push_back(boost::make_tuple(m(0,1),m(1,1),m(2,1)));
        z_axis.push_back(boost::make_tuple(m(0,2),m(1,2),m(2,2)));
        gp << "set xlabel \"x (right+)\"\nset ylabel \"y (back+)\"\nset zlabel \"z (up+)\"\n";
    }
    else if(plot_orientation==1)
    {
        // x pointing backwards, y left, z up
        x_axis.push_back(boost::make_tuple(-m(1,0), m(0,0), m(2,0)));
        y_axis.push_back(boost::make_tuple(-m(1,1), m(0,1), m(2,1)));
        z_axis.push_back(boost::make_tuple(-m(1,2), m(0,2), m(2,2)));
        gp << "set xlabel \"y (left+)\"\nset ylabel \"x (back+)\"\nset zlabel \"z (up+)\"\n";
    }

    gp << "set title \"" << name << " (xyz = rgb)\" font \"sans, 14\" \n";
    gp << "set xrange [-1.25:1.25]\nset yrange [-1.25:1.25]\nset zrange [-1.25:1.25]\n";
    gp << "set view equal xyz\n";
    gp << "unset key\n";

    gp << "splot '-' using 1:2:3 with lines lw 3 lt rgb \"red\"" <<
           ", '-' using 1:2:3 with lines lw 3 lt rgb \"green\"" <<
           ", '-' using 1:2:3 with lines lw 3 lt rgb \"blue\"\n";
    gp.send1d(x_axis);
    gp.send1d(y_axis);
    gp.send1d(z_axis);
}

void plot_rotation(Gnuplot& gp, const string name, const Quaterniond& q, int plot_mode=1)
{
    plot_rotation(gp, name, q.toRotationMatrix(), plot_mode);
}

void plot_rotation(Gnuplot& gp, const string name, const AngleAxisd& a, int plot_mode=1)
{
    plot_rotation(gp, name, a.toRotationMatrix(), plot_mode);
}
