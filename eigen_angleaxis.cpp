/*
To use:
1. Download Eigen from website
2. `mkdir build; cmake ..; make install`
3. git clone https://github.com/dstahlke/gnuplot-iostream
4. make sure boost is installed
5. g++ eigen_angleaxis.cpp -o angleaxis -I/usr/include/eigen3 -lboost_iostreams -lboost_system -lboost_filesystem (-g for debug)

Notes:
* http://web.cse.ohio-state.edu/~wang.3602/courses/cse5542-2013-spring/6-Transformation_II.pdf
* post-multiply for operations in local frame!!!
* Mirroring: https://stackoverflow.com/questions/32438252/efficient-way-to-apply-mirror-effect-on-quaternion-rotation
* columns of rotation matrix are orthonormal vectors
* https://eigen.tuxfamily.org/dox/group__TutorialGeometry.html

*/

#include "plot_eigen_rotations.h"

using namespace std;
using namespace Eigen;

// angles in radians
AngleAxisd angleaxis_from_euler(double yaw, double pitch, double roll)
{
    Matrix3d rot_mat = (
                       AngleAxisd(yaw, Vector3d::UnitZ()) *
                       AngleAxisd(pitch, Vector3d::UnitY()) *
                       AngleAxisd(roll, Vector3d::UnitX()) //*
                       ).toRotationMatrix();
    AngleAxisd aa(rot_mat);
    return aa;
}


AngleAxisd mirror_angleaxis(const AngleAxisd& a)
{
    // Angle axis mirroring
    // Vector3d v = a.axis();
    // v(0) = -v(0); v(2) = -v(2);
    // AngleAxisd mirrored(a.angle(), v);

    // Euler angle mirroring
    // Vector3d ea = a.toRotationMatrix().eulerAngles(2,1,0);
    // AngleAxisd mirrored = AngleAxisd(
    //                       AngleAxisd(-ea(0), Vector3d::UnitZ()) *
    //                       AngleAxisd(ea(1), Vector3d::UnitY()) *
    //                       AngleAxisd(-ea(2), Vector3d::UnitX())
    //                       );

    // Quaternion mirroring
    Quaterniond q(a);
    Quaterniond q_mirrored(q.z(), q.y(), q.x(), q.w()); // This works

    // Quaterniond q_mirrored(q.w(), q.x(), -q.y(), -q.z());
    // Quaterniond q_mirrored(q.w(), -q.x(), q.y(), -q.z());
    // Quaterniond q_mirrored(q.w(), -q.x(), -q.y(), q.z());
    // Quaterniond q_mirrored(-q.y(), q.z(), -q.w(), q.x());
    // Quaterniond q_mirrored(q.z(), -q.y(), -q.x(), q.w());
    // Quaterniond q_mirrored(q.x(), q.w(), q.z(), q.y());
    // Quaterniond q_mirrored(q.y(), q.z(), q.w(), q.x());
    // Quaterniond q_mirrored(q.x(), q.w(), -q.z(), -q.y());

    AngleAxisd mirrored(q_mirrored);

    //Rotate 180 degrees about z-axis to account for difference in frames from left to right
    mirrored = mirrored * AngleAxisd(M_PI, Vector3d::UnitZ());

    return mirrored;
}

void plot_angleaxis_and_mirrored(string name, const AngleAxisd& a)
{
    Gnuplot gp;

    gp << "set multiplot layout 1, 2\n";
    plot_rotation(gp, name, a);

    AngleAxisd mir = mirror_angleaxis(a);
    plot_rotation(gp, "m180 "+name, mir);
}

int main()
{
    const double pi = M_PI;

    AngleAxisd no_rot = angleaxis_from_euler(0, 0, 0);
    AngleAxisd z_rot = angleaxis_from_euler(M_PI/2, 0, 0);

    // These two should be the same when mirrored
    AngleAxisd up = angleaxis_from_euler(-pi/2, 0, pi);
    plot_angleaxis_and_mirrored("up", up);
    AngleAxisd forward = angleaxis_from_euler(-pi/2, 0, pi/2);
    plot_angleaxis_and_mirrored("forward", forward);

    AngleAxisd left = angleaxis_from_euler(0, 0, pi/2);
    plot_angleaxis_and_mirrored("left", left);

    AngleAxisd down = angleaxis_from_euler(0, 0, 0);
    plot_angleaxis_and_mirrored("down", down);

    AngleAxisd left_forward = angleaxis_from_euler(-pi/4, 0, pi/2);
    plot_angleaxis_and_mirrored("left forward", left_forward);

    AngleAxisd up_turnin = angleaxis_from_euler(-3*pi/4, 0, pi);
    plot_angleaxis_and_mirrored("up turnin", up_turnin);

    AngleAxisd down_turnin = angleaxis_from_euler(-pi/4, 0, 0);
    plot_angleaxis_and_mirrored("down turnin", down_turnin);

    AngleAxisd weird = angleaxis_from_euler(0, pi/2, 0);
    plot_angleaxis_and_mirrored("weird", weird);

    // Eigen::Matrix3d test =  //m_human2hand *
    //                            Eigen::Matrix3d(Eigen::AngleAxisd(M_PI/2, Vector3d::UnitX())
    //                                            * Eigen::AngleAxisd(-M_PI/2, Vector3d::UnitZ()));
    // Eigen::Matrix3d testrot = test * Eigen::Matrix3d(Eigen::AngleAxisd(M_PI, Vector3d::UnitZ()));
    //
    // // AngleAxisd test = AngleAxisd(Eigen::Matrix3d(Eigen::AngleAxisd(M_PI/2, Vector3d::UnitX())));
    // //
    // Gnuplot gp;
    // plot_rotation(gp, "test", test);
    // Gnuplot gp2;
    // plot_rotation(gp2, "testrot", testrot);

    // system("echo \"test\"");

    return 0;
}
